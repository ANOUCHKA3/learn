# -*- coding: utf-8 -*-
from datetime import datetime
import dateutil.parser

from odoo import http


class CmStock(http.Controller):


    @http.route('/activites/reports/<string:begin>/<string:end>', type='http', auth='user', website=True)
    def list(self, begin, end, **kw):
        print("Begin Type=>", dateutil.parser.parse(begin))
        print("End =>", end)
        moves = http.request.env['stock.move.line'].search([('date','>',dateutil.parser.parse(begin)) and ('date','<',dateutil.parser.parse(end))])
        products = http.request.env['product.product'].search([])
        purchase_lines = http.request.env['purchase.order.line'].search([])
        stock_quants = http.request.env['stock.quant'].search([])
        lots =http.request.env['stock.production.lot'].search([])
        partners= http.request.env['res.partner'].search([])

        tmp_stock_quants = stock_quants.filtered(lambda x: x.location_id.name != 'Vendors')

        moves.filtered(lambda x: x.location_id.name != 'Vendors')
        print("Moves D1",len(moves))
        for m in moves:
            print("Type date",type(m.date))
        moves.filtered(lambda x:x.date>dateutil.parser.parse(begin))
        moves.filtered(lambda x:x.date<dateutil.parser.parse(end))

        print("Moves D2",len(moves))
        #tmp=list(filter(lambda x:x.date>= dateutil.parser.parse(begin),moves))
        #tmp=list(filter(lambda x:x.date<= dateutil.parser.parse(end),moves))
        print("Moves N",len(tmp))

        print("Stock Quants D",len(tmp_stock_quants))
        if len(moves) == 0: 
            return http.request.render('cm_stock.table')

        else:
                 for m in moves:
                         if m.date >= dateutil.parser.parse(begin) and m.date <=  dateutil.parser.parse(end):
                            print(m.date,'=>',m.product_id.id)

                                #print("move prod=>",m.product_id.id)
                            #tmp_stock_quants=tmp_stock_quants.filtered(lambda x: x.product_id.id == m.product_id.id)

                                #print("Stock Quants N",len(tmp_stock_quants))

                            for s in tmp_stock_quants:
                                numenter = http.request.env['stock.move.line'].search([("product_id", "=", s.product_id.id)])
                                for l in numenter:
                                    if  s.lot_id == l.lot_id:
                                        s.reference = l.reference
                                        s.produit = l.product_id.name
                                        s.number_entered = l.qty_done
                                        s.lot =l.lot_name
                                        s.stock_initial = l.qty_done

                            for s in tmp_stock_quants:
                               nument = http.request.env['stock.picking'].search([("product_id", "=", s.product_id.id)])
                               for i in nument:
                                  if s.reference == i.name:
                                     s.partner = i.partner_id.name

                            for s in tmp_stock_quants:
                               nument = http.request.env['sale.order.line'].search([("product_id", "=", s.product_id.id)])
                               for i in nument:
                                  if type == "TRANSFERTS EXTERNES":
                                      if s.stock_initial == i.product_uom_qty:
                                         s.number_out = i.product_uom_qty


                            for s in tmp_stock_quants:
                                s.stock_final=(s.stock_initial+s.number_entered)-(s.number_out+s.rebut)
                                s.ecart=s.stock_final-s.stock_initial

                            return http.request.render('cm_stock.table', {'cm_stock_quants': tmp_stock_quants})

            #     @http.route('/cm_stock/cm_stock/objects/', auth='public')
    #     def list(self, **kw):
    #         return http.request.render('cm_stock.listing', {
    #             'root': '/cm_stock/cm_stock',
    #             'objects': http.request.env['cm_stock.cm_stock'].search([]),
    #         })


    #     @http.route('/cm_stock/cm_stock/objects/<model("cm_stock.cm_stock"):obj>/', auth='public')
    #     def object(self, obj, **kw):
    #         return http.request.render('cm_stock.object', {
    #             'object': obj
    #         })
